#include <iostream>

using namespace std;

void bubbleSort(int(&array)[30], int n);
void selectionSort(int(&array)[30], int n);
void intertionSort(int(&array)[30], int n);
int enterArray(int(&array)[30], int);
void displayArray(int[], int);
void swap(int&, int&);

int main()
{
    const int length = 30;
    int numbers[length]{ 0 };
    const int newLength = enterArray(numbers, length);
    displayArray(numbers, newLength);
    intertionSort(numbers, newLength);
    cout << endl;
    displayArray(numbers, newLength);
}

int enterArray(int(&numbers)[30], int length)
{
    int new_length;
    cout << "Enter number less than 30: " << endl;
    cin >> new_length;

    for (size_t i = 0; i < new_length; i++)
    {
        cout << "Enter " << i + 1 << "'s element: ";
        cin >> numbers[i];
    }
    return new_length;
}

void displayArray(int numbers[], int length)
{
    for (size_t i = 0; i < length; i++)
    {
        cout << numbers[i] << " ";
    }
}

void bubbleSort(int(&array)[30], int n)
{
    for (int i = 1; i <= n - 1; i++)
    {
        for (int j = 0; j < n - i; j++)
        {
            if (array[j] > array[j + 1])
            {
                swap(array[j], array[j + 1]);
            }
        }
    }
}
void selectionSort(int(&array)[30], int n)
{
    for (int k = 0; k < n - 1; k++)
    {
        int imin = k;
        for (int i = k + 1; i < n; i++)
        {
            if (array[i] < array[imin])
            {
                swap(array[i], array[imin]);
            }
        }

    }
}

void intertionSort(int(&array)[30], int n)
{
    for (int i = 1; i < n; i++)
    {
        int key = array[i];
        int j = i - 1;
        for (j = i - 1; j >= 0 && array[j] > key; j--)
        {
            array[j + 1] = array[j];
        }
        array[j + 1] = key;
    }
}

void swap(int& first, int& second)
{
    int tmp = first;
    first = second;
    second = tmp;
}

