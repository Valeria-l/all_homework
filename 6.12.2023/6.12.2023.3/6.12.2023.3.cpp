#include <iostream>

using namespace std;


bool isEquivalent(const char str1[], const char str2[]);

int main()
{
    const char* str1 = "hello"; // указатель
    const char* str2 = "olleh";
    if (isEquivalent(str1, str2))
    {
        cout << "Strings equalent";
    }
    else
    {
        cout << "Strings not equalent";
    }

}


bool isEquivalent(const char str1[], const char str2[])
{
    int i;
    for ( i = 0; str1[i] && str2[i]; i++)
    {
        if (str1[i] != str2[i])
        {
            return false;
        }
    }
    return true;
}
