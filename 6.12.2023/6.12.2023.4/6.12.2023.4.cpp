#include <iostream>

using namespace std;

int compareStrings(const char* str1, const char* str2) {
    int len1 = strlen(str1);// �������� ����� ������ 1
    int len2 = strlen(str2);// �������� ����� ������ 2
    int minLength = (len1 < len2) ? len1 : len2; // ����������� ����������� ��������

    for (int i = 0; i < minLength; ++i) { // ���������� �� ����������� �����
        if (str1[i] < str2[i])
            return -1; 
        else if (str1[i] > str2[i])
            return 1; 
    }
    

    if (len1 < len2)
    {
        return -1;
    }
    else if (len1 > len2)
    {
        return 1;
    }

    return 0; 
}

int main() {
    const char* str1 = "abc";
    const char* str2 = "xyz";

    int result = compareStrings(str1, str2);

    if (result < 0)
        cout << "str1 < str2" << endl;
    else if (result > 0)
        std::cout << "str1 > str2" << std::endl;
    else
        std::cout << "str1 = str2" << std::endl;

    return 0;
}

