#include <iostream>

using namespace std;

int fibonacci(int n);
    

int main() {
    int n;
    cout << "Enter the number of the number ";
    cin >> n;
    int result = fibonacci(n);
    cout << "The fibonacci number under the number " << n << " equally: " << result << endl;

    return 0;
}

int fibonacci(int n) {
    if (n <= 1) {
        return n;
    }
    else {
        return fibonacci(n - 1) + fibonacci(n - 2);
    }
}
