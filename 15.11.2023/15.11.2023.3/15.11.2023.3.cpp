#include <iostream>

using namespace std;

bool isPalindrome(int n, int reversed);


int main()
{
	int n;
	cout << "Enter the integer number: ";
	cin >> n;

	if (isPalindrome(n, 0))
	{
		cout << "Number is palindrome";
	}
	else
	{
		cout << "Number is not palindrome";
	}
	return 0;
}


bool isPalindrome(int n, int reversed)
{
	if (n == 0)
	{
		return (n == reversed);
	}
	reversed = reversed * 10 + n % 10;
	n /= 10;
	return isPalindrome(n, reversed);

}